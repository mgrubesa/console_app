﻿using Models;
using System;
using System.Collections.Generic;

namespace Service
{
    public class CEOService
    {
        #region Fields

        public Storage<CEO> storeCEO = new Storage<CEO>();

        #endregion Fields

        #region Methods

        public void Add(String FirstName, String LastName, int Age, int CeoYears)
        {
            foreach (CEO c in storeCEO.Get())
            {
                int count = storeCEO.Get().Count;
                if (count == 1)
                {
                    Console.Write("CEO already exists, there can only be one! ");
                    return;
                }
            }
            CEO ceo = new CEO();

            ceo.FirstName = FirstName;

            ceo.LastName = LastName;

            ceo.Age = Age;

            ceo.CeoYears = CeoYears;

            storeCEO.Add(ceo);
        }

        public List<CEO> Get()
        {
            return storeCEO.Get();
        }

        public void Remove(String FirstName, String LastName, int Age)
        {
            storeCEO.Remove(FirstName, LastName, Age);
        }

        #endregion Methods
    }
}