﻿using Models;
using System;
using System.Collections.Generic;

namespace Service
{
    public class Storage<T> where T : BaseModel
    {
        #region Fields

        public List<T> list;

        #endregion Fields

        #region Constructors

        public Storage()
        {
            list = new List<T>();
        }

        #endregion Constructors

        #region Methods

        public void Add(T model)
        {
            list.Add(model);
        }

        public List<T> Get()
        {
            return list;
        }

        public void Remove(String FirstName, String LastName, int Age)
        {
            list.RemoveAll(x => x.FirstName == FirstName && x.LastName == LastName && x.Age == Age);
        }

        #endregion Methods
    }
}