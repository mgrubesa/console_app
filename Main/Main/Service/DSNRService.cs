﻿using Models;
using System;
using System.Collections.Generic;

namespace Service
{
    public class DSNRService
    {
        #region Fields

        public Storage<Designer> storeDSNR = new Storage<Designer>();

        #endregion Fields

        #region Methods

        public void Add(String FirstName, String LastName, int Age, String Project)
        {
            Designer DSNR = new Designer();

            DSNR.FirstName = FirstName;

            DSNR.LastName = LastName;

            DSNR.Age = Age;

            DSNR.CanDraw = true;

            DSNR.Project = Project;

            storeDSNR.Add(DSNR);
        }

        public List<Designer> Get()
        {
            return storeDSNR.Get();
        }

        public void Remove(String FirstName, String LastName, int Age)
        {
            storeDSNR.Remove(FirstName, LastName, Age);
        }

        #endregion Methods
    }
}