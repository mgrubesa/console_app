﻿using Models;
using System;
using System.Collections.Generic;

namespace Service
{
    public class STService
    {
        #region Fields

        public static Storage<SoftwareTester> storeST = new Storage<SoftwareTester>();

        #endregion Fields

        #region Methods

        public void Add(String FirstName, String LastName, int Age, String Project)
        {
            SoftwareTester ST = new SoftwareTester();

            ST.FirstName = FirstName;

            ST.LastName = LastName;

            ST.Age = Age;

            ST.Project = Project;

            ST.UsesAutomatedTests = true;

            storeST.Add(ST);
        }

        public List<SoftwareTester> Get()
        {
            return storeST.Get();
        }

        public void Remove(String FirstName, String LastName, int Age)
        {
            storeST.Remove(FirstName, LastName, Age);
        }

        #endregion Methods
    }
}