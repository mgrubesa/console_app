﻿using Models;
using System;
using System.Collections.Generic;

namespace Service
{
    public class DEVService
    {
        #region Fields

        public Storage<Developer> storeDev = new Storage<Developer>();

        #endregion Fields

        #region Methods

        public void Add(String FirstName, String LastName, int Age, String Project)
        {
            Developer Dev = new Developer();

            Dev.FirstName = FirstName;

            Dev.LastName = LastName;

            Dev.Age = Age;

            Dev.IsStudent = false;

            Dev.Project = Project;

            storeDev.Add(Dev);
        }

        public List<Developer> Get()
        {
            return storeDev.Get();
        }

        public void Remove(String FirstName, String LastName, int Age)
        {
            storeDev.Remove(FirstName, LastName, Age);
        }

        #endregion Methods
    }
}