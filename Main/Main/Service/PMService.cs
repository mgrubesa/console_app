﻿using Models;
using System;
using System.Collections.Generic;

namespace Service
{
    public class PMService

    {
        #region Fields

        public Storage<ProjectManager> storePM = new Storage<ProjectManager>();

        #endregion Fields

        #region Methods

        public void Add(String FirstName, String LastName, int Age, String Project)
        {
            ProjectManager PM = new ProjectManager();

            PM.FirstName = FirstName;

            PM.LastName = LastName;

            PM.Age = Age;

            PM.Project = Project;

            storePM.Add(PM);
        }

        public List<ProjectManager> Get()
        {
            return storePM.Get();
        }

        public void Remove(String FirstName, String LastName, int Age)
        {
            storePM.Remove(FirstName, LastName, Age);
        }

        #endregion Methods
    }
}