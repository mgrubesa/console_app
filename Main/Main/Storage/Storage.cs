﻿using Models;
using System;

namespace Storage
{
    public class Storage
    {
        #region Fields

        private RolesList list = new RolesList();

        #endregion Fields

        #region Methods

        public void StoreAll(BaseModel model)
        {
            list.BMList.Add(model);
        }

        public void StoreCEO(CEO ceo)
        {
            foreach (CEO c in list.CEOList)
            {
                int count = list.CEOList.Count;
                if (count == 1)
                {
                    Console.Write("CEO already exists, there can only be one! ");
                    return;
                }
            }
            list.CEOList.Add(ceo);
        }

        public void StoreDEV(Developer DEV)
        {
            list.DEVList.Add(DEV);
        }

        public void StoreDSNR(Designer DSNR)
        {
            list.DSNRList.Add(DSNR);
        }

        public void StorePM(ProjectManager PM)
        {
            list.PMList.Add(PM);
        }

        public void StoreST(SoftwareTester ST)
        {
            list.STList.Add(ST);
        }

        #endregion Methods
    }
}