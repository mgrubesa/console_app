﻿using Models;
using System;
using System.Linq;

namespace Mono_LV1
{
    public class ServiceImplementation
    {
        #region Fields

        private Services services;

        #endregion Fields

        #region Constructors

        public ServiceImplementation()
        {
            services = new Services();
        }

        #endregion Constructors

        #region Methods

        public void Add(String Role)
        {
            switch (Role.ToUpperInvariant())
            {
                case "CEO":
                    {
                        Console.Write("FirstName: ");
                        String FirstName = Console.ReadLine();
                        Console.Write("LastName: ");
                        String LastName = Console.ReadLine();

                        if (String.IsNullOrEmpty(FirstName) || String.IsNullOrEmpty(LastName))
                        {
                            Console.WriteLine("Empty entry!");
                            return;
                        }

                        Console.Write("Age: ");
                        int Age;
                        if (!int.TryParse(Console.ReadLine(), out Age))
                        {
                            Console.WriteLine("Not a valid age");
                            return;
                        }

                        Console.Write("CeoYears: ");
                        int CeoYears;
                        if (!int.TryParse(Console.ReadLine(), out CeoYears))
                        {
                            Console.WriteLine("Not a valid age");
                            return;
                        }

                        services.CEOService.Add(FirstName, LastName, Age, CeoYears);

                        Console.WriteLine("User added");
                        break;
                    }

                case "DEV":
                    {
                        Console.Write("FirstName: ");
                        String FirstName = Console.ReadLine();
                        Console.Write("LastName: ");
                        String LastName = Console.ReadLine();

                        Console.Write("Project: "); String Project = Console.ReadLine();

                        if (String.IsNullOrEmpty(FirstName) || String.IsNullOrEmpty(LastName) ||
                        String.IsNullOrEmpty(Project)) { Console.WriteLine("Empty entry!"); return; }
                        Console.Write("Age: ");
                        int Age; if (!int.TryParse(Console.ReadLine(), out Age))
                        {
                            Console.WriteLine("Not a valid age");
                            return;
                        }

                        services.DEVService.Add(FirstName, LastName, Age, Project); Console.WriteLine("User added");
                        break;
                    }

                case "DSNR":
                    {
                        Console.Write("FirstName: ");
                        String FirstName = Console.ReadLine();
                        Console.Write("LastName: ");
                        String LastName = Console.ReadLine();

                        Console.Write("Project: "); String Project = Console.ReadLine();

                        if (String.IsNullOrEmpty(FirstName) || String.IsNullOrEmpty(LastName) ||
                        String.IsNullOrEmpty(Project)) { Console.WriteLine("Empty entry!"); return; }
                        Console.Write("Age: ");
                        int Age; if (!int.TryParse(Console.ReadLine(), out Age))
                        {
                            Console.WriteLine("Not a valid age");
                            return;
                        }
                        services.DSNRService.Add(FirstName, LastName, Age, Project);
                        Console.WriteLine("User added"); break;
                    }

                case "ST":
                    {
                        Console.Write("FirstName: ");
                        String FirstName = Console.ReadLine();
                        Console.Write("LastName: ");
                        String LastName = Console.ReadLine();

                        Console.Write("Project: "); String Project = Console.ReadLine();

                        if (String.IsNullOrEmpty(FirstName) || String.IsNullOrEmpty(LastName) ||
                        String.IsNullOrEmpty(Project)) { Console.WriteLine("Empty entry!"); return; }
                        Console.Write("Age: ");
                        int Age; if (!int.TryParse(Console.ReadLine(), out Age))
                        {
                            Console.WriteLine("Not a valid age");
                            return;
                        }
                        services.STService.Add(FirstName, LastName, Age, Project);
                        Console.WriteLine("User added"); break;
                    }

                case "PM":
                    {
                        Console.Write("FirstName: ");
                        String FirstName = Console.ReadLine();
                        Console.Write("LastName: ");
                        String LastName = Console.ReadLine();

                        Console.Write("Project: "); String Project = Console.ReadLine();

                        if (String.IsNullOrEmpty(FirstName) || String.IsNullOrEmpty(LastName) ||
                        String.IsNullOrEmpty(Project)) { Console.WriteLine("Empty entry!"); return; }
                        Console.Write("Age: ");
                        int Age; if (!int.TryParse(Console.ReadLine(), out Age))
                        {
                            Console.WriteLine("Not a valid age");
                            return;
                        }
                        services.PMService.Add(FirstName, LastName, Age, Project);

                        Console.WriteLine("User added"); break;
                    }

                default:

                    Console.WriteLine("No such role");
                    break;
            }
        }

        public void CEOList()
        {
            foreach (CEO p in services.CEOService.Get())
            {
                Console.WriteLine(p.LastName + ", " + p.FirstName + ", " + p.Age + ", " + p.CeoYears);
            }
        }

        public void DEVList()
        {
            foreach (Developer p in services.DEVService.Get())
            {
                Console.WriteLine(p.LastName + ", " + p.FirstName + ", " + p.Age + ", " + p.Project);
            }
        }

        public void Display()
        {
            Console.WriteLine("Employees: ");
            foreach (CEO p in services.CEOService.Get())
            {
                Console.WriteLine(p.GetType().Name + ", " + p.LastName + ", " + p.FirstName + ", " + p.Age);
            }

            foreach (Developer p in services.DEVService.Get())
            {
                Console.WriteLine(p.GetType().Name + ", " + p.LastName + ", " + p.FirstName + ", " + p.Age);
            }
            foreach (Designer p in services.DSNRService.Get())
            {
                Console.WriteLine(p.GetType().Name + ", " + p.LastName + ", " + p.FirstName + ", " + p.Age);
            }

            foreach (SoftwareTester p in services.STService.Get())
            {
                Console.WriteLine(p.GetType().Name + ", " + p.LastName + ", " + p.FirstName + ", " + p.Age);
            }

            foreach (ProjectManager p in services.PMService.Get())
            {
                Console.WriteLine(p.GetType().Name + ", " + p.LastName + ", " + p.FirstName + ", " + p.Age);
            }
        }

        public void DSNRList()
        {
            foreach (Designer p in services.DSNRService.Get())
            {
                Console.WriteLine(p.LastName + ", " + p.FirstName + ", " + p.Age + ", " + p.Project);
            }
        }

        public void Help()
        {
            Console.WriteLine("Available commands: Add, Remove, Display, List, <role_name>List");
        }

        public void List()
        {
            foreach (Developer p in services.DEVService.Get())
            {
                Console.WriteLine(p.GetType().Name + ", " + p.LastName + ", " + p.FirstName + ", " + p.Age);
            }
            foreach (Designer p in services.DSNRService.Get())
            {
                Console.WriteLine(p.GetType().Name + ", " + p.LastName + ", " + p.FirstName + ", " + p.Age);
            }

            foreach (SoftwareTester p in services.STService.Get())
            {
                Console.WriteLine(p.GetType().Name + ", " + p.LastName + ", " + p.FirstName + ", " + p.Age);
            }

            foreach (ProjectManager p in services.PMService.Get())
            {
                Console.WriteLine(p.GetType().Name + ", " + p.LastName + ", " + p.FirstName + ", " + p.Age);
            }
        }

        public void PMList()
        {
            foreach (ProjectManager p in services.PMService.Get())
            {
                Console.WriteLine(p.LastName + ", " + p.FirstName + ", " + p.Age + ", " + p.Project);
            }
        }

        public void Remove(String Role)
        {
            switch (Role.ToUpperInvariant())
            {
                case "CEO":
                    {
                        Console.Write("FirstName: ");
                        String FirstName = Console.ReadLine();
                        Console.Write("LastName: ");
                        String LastName = Console.ReadLine();

                        if (String.IsNullOrEmpty(FirstName) || String.IsNullOrEmpty(LastName))
                        {
                            Console.WriteLine("Empty entry!");
                            return;
                        }

                        Console.Write("Age: ");
                        int Age;
                        if (!int.TryParse(Console.ReadLine(), out Age))
                        {
                            Console.WriteLine("Not a valid age");
                            return;
                        }

                        if (services.CEOService.Get().Any(x => x.Age == Age && x.FirstName == FirstName && x.LastName == LastName))
                        {
                            services.CEOService.Remove(FirstName, LastName, Age);
                            Console.WriteLine("User removed");
                        }
                        else
                        {
                            Console.WriteLine("This user does not exist!");
                        }

                        break;
                    }
                case "DEV":
                    {
                        Console.Write("FirstName: ");
                        String FirstName = Console.ReadLine();
                        Console.Write("LastName: ");
                        String LastName = Console.ReadLine();
                        Console.Write("Age: ");
                        int Age = int.Parse(Console.ReadLine());

                        if (services.DEVService.Get().Any(x => x.Age == Age && x.FirstName == FirstName &&
                        x.LastName == LastName))
                        {
                            services.DEVService.Remove(FirstName, LastName, Age);

                            Console.WriteLine("User removed");
                        }
                        else { Console.WriteLine("This user does not exist!"); }

                        break;
                    }

                case "DSNR":
                    {
                        Console.Write("FirstName: ");
                        String FirstName = Console.ReadLine();
                        Console.Write("LastName: ");
                        String LastName = Console.ReadLine();

                        if (String.IsNullOrEmpty(FirstName) || String.IsNullOrEmpty(LastName))
                        {
                            Console.WriteLine("Empty entry!"); return;
                        }

                        Console.Write("Age: "); int Age; if (!int.TryParse(Console.ReadLine(), out Age))
                        {
                            Console.WriteLine("Not a valid age"); return;
                        }

                        if (services.DSNRService.Get().Any(x => x.Age == Age && x.FirstName == FirstName &&
                        x.LastName == LastName))
                        {
                            services.DSNRService.Remove(FirstName, LastName, Age);

                            Console.WriteLine("User removed");
                        }
                        else { Console.WriteLine("This user does not exist!"); }

                        break;
                    }

                case "ST":
                    {
                        Console.Write("FirstName: ");
                        String FirstName = Console.ReadLine();
                        Console.Write("LastName: ");
                        String LastName = Console.ReadLine();

                        if (String.IsNullOrEmpty(FirstName) || String.IsNullOrEmpty(LastName))
                        {
                            Console.WriteLine("Emptyentry!"); return;
                        }

                        Console.Write("Age: "); int Age; if (!int.TryParse(Console.ReadLine(), out Age))
                        {
                            Console.WriteLine("Not a valid age"); return;
                        }

                        if (services.STService.Get().Any(x => x.Age == Age && x.FirstName == FirstName &&
                        x.LastName == LastName))
                        {
                            services.STService.Remove(FirstName, LastName, Age);

                            Console.WriteLine("User removed");
                        }
                        else { Console.WriteLine("This user does not exist!"); }

                        break;
                    }

                case "PM":
                    {
                        Console.Write("FirstName: ");
                        String FirstName = Console.ReadLine();
                        Console.Write("LastName: ");
                        String LastName = Console.ReadLine();

                        if (String.IsNullOrEmpty(FirstName) || String.IsNullOrEmpty(LastName))
                        {
                            Console.WriteLine("Empty entry!"); return;
                        }

                        Console.Write("Age: "); int Age; if (!int.TryParse(Console.ReadLine(), out Age))
                        {
                            Console.WriteLine("Not a valid age"); return;
                        }

                        if (services.PMService.Get().Any(x => x.Age == Age && x.FirstName == FirstName &&
                        x.LastName == LastName))
                        {
                            services.PMService.Remove(FirstName, LastName, Age);

                            Console.WriteLine("User removed");
                        }
                        else { Console.WriteLine("This user does not exist!"); }
                        break;
                    }

                default:
                    Console.WriteLine("No such role");
                    break;
            }
        }

        public void STList()
        {
            foreach (SoftwareTester p in services.STService.Get())
            {
                Console.WriteLine(p.LastName + ", " + p.FirstName + ", " + p.Age + ", " + p.Project);
            }
        }

        #endregion Methods
    }
}