﻿using System;

namespace Mono_LV1
{
    internal class Program
    {
        #region Methods

        private static void Main(string[] args)

        {
            Company company = new Company();

            company.Help();
            String command;

            do
            {
                Console.Write("Command: ");

                command = Console.ReadLine().ToUpperInvariant();

                if (String.Equals(command, Commands.Help))
                {
                    company.Help();
                }
                else if (String.Equals(command, Commands.List))
                {
                    company.List();
                }
                else if (String.Equals(command, Commands.Display))
                {
                    company.Display();
                }
                else if (String.Equals(command, Commands.CeoList))
                {
                    company.CEOList();
                }
                else if (String.Equals(command, Commands.DevList))
                {
                    company.DEVList();
                }
                else if (String.Equals(command, Commands.DsnrList))
                {
                    company.DSNRList();
                }
                else if (String.Equals(command, Commands.StList))
                {
                    company.STList();
                }
                else if (String.Equals(command, Commands.PmList))
                {
                    company.PMList();
                }
                else if (String.Equals(command, Commands.Add))
                {
                    Console.Write("Role: ");
                    company.Add(Console.ReadLine());
                }
                else if (String.Equals(command, Commands.Remove))
                {
                    Console.Write("Role: ");
                    company.Remove(Console.ReadLine());
                }
                else
                {
                    if (command != "EXIT")
                    {
                        Console.WriteLine("No such command");
                    }
                }

                Console.WriteLine();
            } while (command != "EXIT");
        }

        #endregion Methods
    }
}