﻿using Service;

namespace Mono_LV1
{
    public class Services
    {
        #region Constructors

        public Services()
        {
            CEOService = new CEOService();
            DEVService = new DEVService();
            PMService = new PMService();
            DSNRService = new DSNRService();
            STService = new STService();
        }

        #endregion Constructors

        #region Properties

        public CEOService CEOService { get; set; }
        public DEVService DEVService { get; set; }
        public DSNRService DSNRService { get; set; }
        public PMService PMService { get; set; }
        public STService STService { get; set; }

        #endregion Properties
    }
}