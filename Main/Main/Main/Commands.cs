﻿namespace Mono_LV1
{
    public static class Commands
    {
        #region Fields

        public const string Add = "ADD";
        public const string CeoList = "CEOLIST";
        public const string DevList = "DEVLIST";
        public const string Display = "DISPLAY";
        public const string DsnrList = "DSNRLIST";
        public const string Exit = "EXIT";
        public const string Help = "HELP";
        public const string List = "LIST";
        public const string PmList = "PMLIST";
        public const string Remove = "REMOVE";
        public const string StList = "STLIST";

        #endregion Fields
    }
}