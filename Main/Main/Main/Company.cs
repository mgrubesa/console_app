﻿using Models;
using Service;
using System;
using System.Linq;

namespace Mono_LV1
{
    public class Company
    {
        #region Fields

        private CEOService ceoService = new CEOService();
        private DEVService devService = new DEVService();
        private DSNRService dsnrService = new DSNRService();
        private PMService pmService = new PMService();
        private STService stService = new STService();

        #endregion Fields

        #region Methods

        public void Add(String Role)
        {
            switch (Role.ToUpperInvariant())
            {
                case "CEO":
                    {
                        Console.Write("FirstName: ");
                        String FirstName = Console.ReadLine();
                        Console.Write("LastName: ");
                        String LastName = Console.ReadLine();

                        if (String.IsNullOrEmpty(FirstName) || String.IsNullOrEmpty(LastName))
                        {
                            Console.WriteLine("Empty entry!");
                            return;
                        }

                        Console.Write("Age: ");
                        int Age;
                        if (!int.TryParse(Console.ReadLine(), out Age))
                        {
                            Console.WriteLine("Not a valid age");
                            return;
                        }

                        Console.Write("CeoYears: ");
                        int CeoYears;
                        if (!int.TryParse(Console.ReadLine(), out CeoYears))
                        {
                            Console.WriteLine("Not a valid age");
                            return;
                        }

                        ceoService.Add(FirstName, LastName, Age, CeoYears);

                        Console.WriteLine("User added");
                        break;
                    }

                case "DEV":
                    {
                        Console.Write("FirstName: ");
                        String FirstName = Console.ReadLine();
                        Console.Write("LastName: ");
                        String LastName = Console.ReadLine();

                        Console.Write("Project: "); String Project = Console.ReadLine();

                        if (String.IsNullOrEmpty(FirstName) || String.IsNullOrEmpty(LastName) ||
                        String.IsNullOrEmpty(Project)) { Console.WriteLine("Empty entry!"); return; }
                        Console.Write("Age: ");
                        int Age; if (!int.TryParse(Console.ReadLine(), out Age))
                        {
                            Console.WriteLine("Not a valid age");
                            return;
                        }

                        devService.Add(FirstName, LastName, Age, Project); Console.WriteLine("User added");
                        break;
                    }

                case "DSNR":
                    {
                        Console.Write("FirstName: ");
                        String FirstName = Console.ReadLine();
                        Console.Write("LastName: ");
                        String LastName = Console.ReadLine();

                        Console.Write("Project: "); String Project = Console.ReadLine();

                        if (String.IsNullOrEmpty(FirstName) || String.IsNullOrEmpty(LastName) ||
                        String.IsNullOrEmpty(Project)) { Console.WriteLine("Empty entry!"); return; }
                        Console.Write("Age: ");
                        int Age; if (!int.TryParse(Console.ReadLine(), out Age))
                        {
                            Console.WriteLine("Not a valid age");
                            return;
                        }
                        dsnrService.Add(FirstName, LastName, Age, Project);
                        Console.WriteLine("User added"); break;
                    }

                case "ST":
                    {
                        Console.Write("FirstName: ");
                        String FirstName = Console.ReadLine();
                        Console.Write("LastName: ");
                        String LastName = Console.ReadLine();

                        Console.Write("Project: "); String Project = Console.ReadLine();

                        if (String.IsNullOrEmpty(FirstName) || String.IsNullOrEmpty(LastName) ||
                        String.IsNullOrEmpty(Project)) { Console.WriteLine("Empty entry!"); return; }
                        Console.Write("Age: ");
                        int Age; if (!int.TryParse(Console.ReadLine(), out Age))
                        {
                            Console.WriteLine("Not a valid age");
                            return;
                        }
                        stService.Add(FirstName, LastName, Age, Project);
                        Console.WriteLine("User added"); break;
                    }

                case "PM":
                    {
                        Console.Write("FirstName: ");
                        String FirstName = Console.ReadLine();
                        Console.Write("LastName: ");
                        String LastName = Console.ReadLine();

                        Console.Write("Project: "); String Project = Console.ReadLine();

                        if (String.IsNullOrEmpty(FirstName) || String.IsNullOrEmpty(LastName) ||
                        String.IsNullOrEmpty(Project)) { Console.WriteLine("Empty entry!"); return; }
                        Console.Write("Age: ");
                        int Age; if (!int.TryParse(Console.ReadLine(), out Age))
                        {
                            Console.WriteLine("Not a valid age");
                            return;
                        }
                        pmService.Add(FirstName, LastName, Age, Project);

                        Console.WriteLine("User added"); break;
                    }

                default:

                    Console.WriteLine("No such role");
                    break;
            }
        }

        public void CEOList()
        {
            foreach (CEO p in ceoService.Get())
            {
                Console.WriteLine($"{p.LastName}, {p.FirstName}, {p.Age}, {p.CeoYears}");
            }
        }

        public void DEVList()
        {
            foreach (Developer p in devService.Get())
            {
                Console.WriteLine($"{p.LastName}, {p.FirstName}, {p.Age}, {p.Project}");
            }
        }

        public void Display()
        {
            Console.WriteLine("Employees: ");
            foreach (CEO p in ceoService.Get())
            {
                Console.WriteLine($"{ p.GetType().Name}, {p.LastName}, {p.FirstName}, {p.Age}");
            }
            foreach (Developer p in devService.Get())
            {
                Console.WriteLine($"{ p.GetType().Name}, {p.LastName}, {p.FirstName}, {p.Age}");
            }
            foreach (Designer p in dsnrService.Get())
            {
                Console.WriteLine($"{ p.GetType().Name}, {p.LastName}, {p.FirstName}, {p.Age}");
            }

            foreach (SoftwareTester p in stService.Get())
            {
                Console.WriteLine($"{ p.GetType().Name}, {p.LastName}, {p.FirstName}, {p.Age}");
            }

            foreach (ProjectManager p in pmService.Get())
            {
                Console.WriteLine($"{ p.GetType().Name}, {p.LastName}, {p.FirstName}, {p.Age}");
            }
        }

        public void DSNRList()
        {
            foreach (Designer p in dsnrService.Get())
            {
                Console.WriteLine($"{p.LastName}, {p.FirstName}, {p.Age}, {p.Project}");
            }
        }

        public void Help()
        {
            Console.WriteLine("Available commands: Add, Remove, Exit, Display, List, <role_name>List");
        }

        public void List()
        {
            foreach (Developer p in devService.Get())
            {
                Console.WriteLine($"{ p.GetType().Name}, {p.LastName}, {p.FirstName}, {p.Age}");
            }
            foreach (Designer p in dsnrService.Get())
            {
                Console.WriteLine($"{ p.GetType().Name}, {p.LastName}, {p.FirstName}, {p.Age}");
            }

            foreach (SoftwareTester p in stService.Get())
            {
                Console.WriteLine($"{ p.GetType().Name}, {p.LastName}, {p.FirstName}, {p.Age}");
            }

            foreach (ProjectManager p in pmService.Get())
            {
                Console.WriteLine($"{ p.GetType().Name}, {p.LastName}, {p.FirstName}, {p.Age}");
            }
        }

        public void PMList()
        {
            foreach (ProjectManager p in pmService.Get())
            {
                Console.WriteLine($"{p.LastName}, {p.FirstName}, {p.Age}, {p.Project}");
            }
        }

        public void Remove(String Role)
        {
            switch (Role.ToUpperInvariant())
            {
                case "CEO":
                    {
                        Console.Write("FirstName: ");
                        String FirstName = Console.ReadLine();
                        Console.Write("LastName: ");
                        String LastName = Console.ReadLine();

                        if (String.IsNullOrEmpty(FirstName) || String.IsNullOrEmpty(LastName))
                        {
                            Console.WriteLine("Empty entry!");
                            return;
                        }

                        Console.Write("Age: ");
                        int Age;
                        if (!int.TryParse(Console.ReadLine(), out Age))
                        {
                            Console.WriteLine("Not a valid age");
                            return;
                        }

                        if (ceoService.Get().Any(x => x.Age == Age && x.FirstName == FirstName && x.LastName == LastName))
                        {
                            ceoService.Remove(FirstName, LastName, Age);
                            Console.WriteLine("User removed");
                        }
                        else
                        {
                            Console.WriteLine("This user does not exist!");
                        }

                        break;
                    }
                case "DEV":
                    {
                        Console.Write("FirstName: ");
                        String FirstName = Console.ReadLine();
                        Console.Write("LastName: ");
                        String LastName = Console.ReadLine();
                        Console.Write("Age: ");
                        int Age = int.Parse(Console.ReadLine());

                        if (devService.Get().Any(x => x.Age == Age && x.FirstName == FirstName &&
                        x.LastName == LastName))
                        {
                            devService.Remove(FirstName, LastName, Age);

                            Console.WriteLine("User removed");
                        }
                        else { Console.WriteLine("This user does not exist!"); }

                        break;
                    }

                case "DSNR":
                    {
                        Console.Write("FirstName: ");
                        String FirstName = Console.ReadLine();
                        Console.Write("LastName: ");
                        String LastName = Console.ReadLine();

                        if (String.IsNullOrEmpty(FirstName) || String.IsNullOrEmpty(LastName))
                        {
                            Console.WriteLine("Empty entry!"); return;
                        }

                        Console.Write("Age: "); int Age; if (!int.TryParse(Console.ReadLine(), out Age))
                        {
                            Console.WriteLine("Not a valid age"); return;
                        }

                        if (dsnrService.Get().Any(x => x.Age == Age && x.FirstName == FirstName &&
                        x.LastName == LastName))
                        {
                            dsnrService.Remove(FirstName, LastName, Age);

                            Console.WriteLine("User removed");
                        }
                        else { Console.WriteLine("This user does not exist!"); }

                        break;
                    }

                case "ST":
                    {
                        Console.Write("FirstName: ");
                        String FirstName = Console.ReadLine();
                        Console.Write("LastName: ");
                        String LastName = Console.ReadLine();

                        if (String.IsNullOrEmpty(FirstName) || String.IsNullOrEmpty(LastName))
                        {
                            Console.WriteLine("Emptyentry!"); return;
                        }

                        Console.Write("Age: "); int Age; if (!int.TryParse(Console.ReadLine(), out Age))
                        {
                            Console.WriteLine("Not a valid age"); return;
                        }

                        if (stService.Get().Any(x => x.Age == Age && x.FirstName == FirstName &&
                        x.LastName == LastName))
                        {
                            stService.Remove(FirstName, LastName, Age);

                            Console.WriteLine("User removed");
                        }
                        else { Console.WriteLine("This user does not exist!"); }

                        break;
                    }

                case "PM":
                    {
                        Console.Write("FirstName: ");
                        String FirstName = Console.ReadLine();
                        Console.Write("LastName: ");
                        String LastName = Console.ReadLine();

                        if (String.IsNullOrEmpty(FirstName) || String.IsNullOrEmpty(LastName))
                        {
                            Console.WriteLine("Empty entry!"); return;
                        }

                        Console.Write("Age: "); int Age; if (!int.TryParse(Console.ReadLine(), out Age))
                        {
                            Console.WriteLine("Not a valid age"); return;
                        }

                        if (pmService.Get().Any(x => x.Age == Age && x.FirstName == FirstName &&
                        x.LastName == LastName))
                        {
                            pmService.Remove(FirstName, LastName, Age);

                            Console.WriteLine("User removed");
                        }
                        else { Console.WriteLine("This user does not exist!"); }
                        break;
                    }

                default:
                    Console.WriteLine("No such role");
                    break;
            }
        }

        public void STList()
        {
            foreach (SoftwareTester p in stService.Get())
            {
                Console.WriteLine($"{p.LastName}, {p.FirstName}, {p.Age}, {p.Project}");
            }
        }

        #endregion Methods
    }
}