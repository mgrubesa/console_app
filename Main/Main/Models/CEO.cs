﻿namespace Models
{
    public class CEO : BaseModel
    {
        #region Properties

        public int CeoYears { get; set; }

        #endregion Properties
    }
}