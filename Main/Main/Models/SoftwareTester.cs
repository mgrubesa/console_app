﻿using System;

namespace Models
{
    public class SoftwareTester : BaseModel
    {
        #region Properties

        public String Project { get; set; }
        public bool UsesAutomatedTests { get; set; }

        #endregion Properties
    }
}