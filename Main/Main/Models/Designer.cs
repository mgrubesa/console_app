﻿using System;

namespace Models
{
    public class Designer : BaseModel
    {
        #region Properties

        public bool CanDraw { get; set; }
        public String Project { get; set; }

        #endregion Properties
    }
}