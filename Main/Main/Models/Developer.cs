﻿using System;

namespace Models
{
    public class Developer : BaseModel
    {
        #region Properties

        public bool IsStudent { get; set; }
        public String Project { get; set; }

        #endregion Properties
    }
}