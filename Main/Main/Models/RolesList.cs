﻿using System.Collections.Generic;

namespace Models
{
    public class RolesList
    {
        #region Fields

        public static List<BaseModel> BMList;
        public static List<CEO> CEOList;
        public static List<Developer> DEVList;
        public static List<Designer> DSNRList;
        public static List<ProjectManager> PMList;
        public static List<SoftwareTester> STList;

        #endregion Fields

        #region Constructors

        public RolesList()
        {
            BMList = new List<BaseModel>();
            CEOList = new List<CEO>();
            DEVList = new List<Developer>();
            DSNRList = new List<Designer>();
            STList = new List<SoftwareTester>();
            PMList = new List<ProjectManager>();
        }

        #endregion Constructors

        #region Methods

        public List<BaseModel> getBaseList()
        {
            return BMList;
        }

        public List<CEO> GetCEOList()
        {
            return CEOList;
        }

        public List<Developer> GetDEVList()
        {
            return DEVList;
        }

        public List<Designer> GetDSNRList()
        {
            return DSNRList;
        }

        public List<ProjectManager> GetPMList()
        {
            return PMList;
        }

        public List<SoftwareTester> GetSTList()
        {
            return STList;
        }

        #endregion Methods
    }
}