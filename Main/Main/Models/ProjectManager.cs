﻿using System;

namespace Models
{
    public class ProjectManager : BaseModel
    {
        #region Properties

        public String Project { get; set; }

        #endregion Properties
    }
}