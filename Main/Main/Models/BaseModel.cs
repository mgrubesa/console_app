﻿using System;

namespace Models
{
    public class BaseModel
    {
        #region Properties

        public int Age { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }

        #endregion Properties
    }
}